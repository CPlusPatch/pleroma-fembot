#!/usr/bin/env python3

import asyncio
from pleroma import Pleroma, BadRequest
import os
from dotenv import load_dotenv

load_dotenv()

class Main:
	def __init__(self, pleroma):
		self.pleroma = pleroma

	async def handleRequest(self, request: str, notif: any, account: str) -> None:
		if checkIfArrayIsIn(request, ("meow", "nya")):
			await self.reply(notif, "Meooww!")

		if checkIfArrayIsIn(request, ("love you", "like you")):
			await self.reply(notif, "I love you too human!!! Keep being great!! 💕💕")

		if checkIfArrayIsIn(request, ("cute bot", "good bot", "nice bot", "cool bot", "you're cool")):
			await self.reply(notif, "Why, thank you! That's a nice thing to say!")	
		
		#if checkIfArrayIsIn(request, ("help", "halp", "how to")):
		#	await self.pleroma.reply(notification["status"], "Here are my commands:")

	async def reply(self, notif, text):
		await self.pleroma.reply(notif['status'], text)

	async def handleNotification(self, notification):
		text = notification["status"]["akkoma"]["source"]["content"]
		account = notification["account"]["fqn"]

		await self.handleRequest(text, notification, account)

def checkIfArrayIsIn(string: str, array: list[str]) -> bool:
	"""
	This loops over an array of strings and checks if that string is inside the first argument.
	If yes, returns True.
	"""
	for n in array:
		if n.lower() in deduplicateConsecutive(string.lower()):
			return True

	return False

def deduplicateConsecutive(string: str) -> str:
	"""
	When given a string, removes consecutive duplicate characters that directly follow each other.
	Example: "meeeeoooow" -> "meow"
	Returns the deduped string.
	"""
	previousChar = ""
	newString = ""

	for char in string:
		if len(newString) == 0:
			newString += char
			previousChar = char
		if char == previousChar:
			continue
		else:
			newString += char
			previousChar = char
	
	return newString

def mk_pleroma() -> Pleroma:
	"""
	Shortcut for creating a Pleroma object quickly
	"""
	server_url = os.getenv("SERVER_URL")
	access_token = os.getenv("ACCESS_TOKEN")
	return Pleroma(api_base_url=server_url, access_token=access_token)

async def main():
	async with mk_pleroma() as pleroma:
		main = Main(pleroma)
		#main.handleRequest("meeoowwww :3")
		while True:
			async for notification in pleroma.stream_mentions():
				await main.handleNotification(notification)
			

if __name__ == "__main__":
	asyncio.run(main()) # use asyncio.run to run with await